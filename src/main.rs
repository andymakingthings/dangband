#![feature(nll)]

use std::{io, io::Write};
use easycurses::*;
use unicode_segmentation::UnicodeSegmentation;

const MAX_MAP_W : usize = 1000;
const MAX_MAP_H : usize = 1000;
fn display_coords(x: usize, y: usize) -> (i32, i32) {
    let adj = MAX_MAP_W/2;
    let x = if x > adj {(x-adj) as i32} else {-((adj-x) as i32)};
    let adj = MAX_MAP_H/2;
    let y = if y > adj {(y-adj) as i32} else {-((adj-y) as i32)};
    (x,y)
}
fn coords(x: i32, y: i32) -> (usize, usize) {
    let adj = MAX_MAP_W/2;
    let x = if x > 0 {adj + (x as usize)} else {adj - ((-x) as usize)};
    let adj = MAX_MAP_H/2;
    let y = if y > 0 {adj + (y as usize)} else {adj - ((-y) as usize)};
    (x,y)
}

struct GameInstance {
    state: GameState,
    display: EasyCurses,
    log: Vec<String>,
}

struct GameState {
    // 2d array (grid)
    map: Vec<Vec<MapPoint>>,
    history: Vec<String>,
    player: Player,
}

struct Player {
    x: usize,
    y: usize,
    health: i32,
}

impl GameState {
    fn add_line(&mut self, s : &str) -> String {
        self.history.push(String::from(s));
        self.history.last().unwrap().to_string()
    }
}

impl GameInstance {
    fn log(&mut self, s : &str) {
        self.state.add_line(s);
        self.print_log(s);
    }
}

struct MapPoint {
    stack: Vec<MapObject>,
}

enum MapObject {
    Knight,
    Fire,
    Wall,
    Rubble,
    Hole,
    Wench,
}

enum Action {
    // general
    Unknown,
    Exit,

    // pointless
    Sigh,

    // info
    Remember,
    ListInventory,

    // movement
    MoveN,
    MoveNE,
    MoveE,
    MoveSE,
    MoveS,
    MoveSW,
    MoveW,
    MoveNW,

    // war
    Punch,
}

impl GameInstance {
    fn write_header(&mut self) {
        let (a,b) = display_coords(self.state.player.x, self.state.player.y);
        self.println(&format!("L: {},{} H: {}", a, b, self.state.player.health));
        self.println(&format!(""));
    }

    fn write_log(&mut self) {
        let mut last_n = 0;
        let (row_count, col_count) = self.display.get_row_col_count();
        let mut rows_remaining = row_count - 3;
        for i in (0..self.log.len()).rev() {
            let lines = count_lines(col_count, 0, &self.log[i]);
            if rows_remaining >= lines {
                rows_remaining -= lines;
                last_n += 1;
            } else {
                break;
            }
        }
        //last_n = self.log.len();
        for i in self.log.len()-last_n..self.log.len() {
            self.println(&format!("{}", self.log[i]));
        }
    }

    fn refresh(&mut self) {
        self.display.clear();
        self.write_header();
        self.write_log();
    }

    fn print_log(&mut self, s: &str) {
        self.log.push(s.to_string());
        self.refresh();
    }

    fn prompt(&mut self) -> Action {
        self.display.print("> ");
        let mut action = self.display.get_line();
        let action = action.trim();
        match action {
            _ if action == "sigh" => Action::Sigh,
            _ if action == "quit" => Action::Exit,
            _ if action == "left" => Action::MoveW,
            _ if action == "right" => Action::MoveE,
            _ if action == "up" => Action::MoveN,
            _ if action == "down" => Action::MoveS,
            _ if action == "remember" => Action::Remember,
            _ => Action::Unknown,
        }
    }
}

trait GetLine {fn get_line(&mut self) -> String;}
impl GetLine for EasyCurses {
    fn get_line(&mut self) -> String {
        let mut line = String::new();
        loop {
            match self.get_input() {
                Some(Input::Character(c)) => {
                    if c == '\n' || c == '\r' {break;}
                    else {line.push(c);}
                },

                None => {break;},
                _ => ()
            };
        }
        line
    }
}

fn new_cursor_position(col_width: i32, c_pos: i32, s: &str) -> i32 {
    let lines = s.split('\n');
    let mut pos = 0;
    let mut line_pos = c_pos;
    let mut r = 0;
    for line in lines {
        let mut i = 0;
        for c in line.graphemes(true) {
            if c == "\r" {
                line_pos = 0;
            } else {
                line_pos += 1;
            }
        }
        pos += line_pos;
        line_pos = pos % col_width;
        r = col_width - line_pos;
        pos += r;
    }
    pos -= r;
    pos
}

fn count_lines(col_width: i32, c_pos: i32, s: &str) -> i32 {
    let mut x = new_cursor_position(col_width, c_pos, s);
    let remainder = x % col_width;
    if remainder > 0 {x = x + col_width - remainder;}
    x/col_width
}

fn count_new_lines(col_width: i32, c_pos: i32, s: &str) -> i32 {
    let x = count_lines(col_width, c_pos, s);
    if x > 0 {x-1} else {x}
}

impl GameInstance {
    fn print<T: std::convert::AsRef<str> + std::fmt::Display>(&mut self, s: T) {
        self.display.print(format!("{}", s));
        self.display.refresh();
    }
    fn println(&mut self, s: &str) {
        self.print(format!("{}\n\r", s));
    }
}

trait ClearLine {fn clear_line(&mut self);}
impl ClearLine for EasyCurses {
    fn clear_line(&mut self) {
        let (row_count, col_count) = self.get_row_col_count();
        let (r, c) = self.get_cursor_rc();
        self.move_rc(r, 0);
        for i in 0..col_count {self.print(" ");}
        self.move_rc(r, 0);
        for i in 0..col_count {self.delete_char();}
        self.refresh();
    }
}

enum Move {
    Up(i32),
    Down(i32),
    Left(i32),
    Right(i32),
    Point((i32, i32)),
}

trait EasyCursesExt {
    fn move_cursor(&mut self, direction: Move) -> bool;
}

impl EasyCursesExt for EasyCurses {
    fn move_cursor(&mut self, direction: Move) -> bool {
        let (r0, c0) = self.get_cursor_rc();
        let (r, c) = match direction {
            Move::Up(x) => (r0-x, c0),
            Move::Down(x) => (r0+x, c0),
            Move::Left(x) => (r0, c0-x),
            Move::Right(x) => (r0, c0+x),
            Move::Point((x,y)) =>(x, y)
        };
        self.move_rc(r, c)
    }
}

impl GameInstance {
    fn new(state: GameState) -> GameInstance {
        let mut game = GameInstance {
            state: state,
            display: EasyCurses::initialize_system().unwrap(),
            log: Vec::new(),
        };
        game.display.set_input_mode(InputMode::Cooked);
        game.display.set_color_pair(ColorPair::new(Color::Green, Color::Black));
        game.display.set_cursor_visibility(CursorVisibility::Invisible);
        game
    }
}

enum ActionResult {
    Nothing,
    Die,
    Pain,
}

impl GameInstance {
    fn do_action(&mut self, action: &Action) -> ActionResult {
        match action {
            Action::Sigh => {
                self.log("You breathe a sigh of relief...");
                ActionResult::Nothing
            },

            Action::Remember => {
                self.print_log("You try to remember what you've been doing...");
                for i in 0..self.state.history.len() {
                    self.print_log(&format!("  ... {}", self.state.history[i]))
                }
                ActionResult::Nothing
            },

            Action::MoveW => {
                self.state.player.x -= 1;
                self.log("You walk west.");
                self.check_collision()
            }
            Action::MoveE => {
                self.state.player.x += 1;
                self.log("You walk east.");
                self.check_collision()
            }
            Action::MoveN => {
                self.state.player.y += 1;
                self.log("You walk north.");
                self.check_collision()
            }
            Action::MoveS => {
                self.state.player.y -= 1;
                self.log("You walk south.");
                self.check_collision()
            }
            _ => ActionResult::Nothing
        }
    }
}

impl GameInstance {
    fn check_collision(&mut self) -> ActionResult {
        let (x,y) = (self.state.player.x, self.state.player.y);
        let mut result = ActionResult::Nothing;
        for i in 0..self.state.map[x][y].stack.len() {
            match self.state.map[x][y].stack[i] {
                MapObject::Knight => {
                    self.log("A Knight smites you.");
                    self.state.player.health -= 4;
                    if self.state.player.health <= 0 {
                        result = ActionResult::Die;
                    } else {
                        self.log("You take it like a man.");
                        result = ActionResult::Pain;
                    }
                },

                MapObject::Fire => {
                    self.log("A fire burns you.");
                    self.state.player.health -= 1;
                    if self.state.player.health <= 0 {
                        result = ActionResult::Die;
                    } else {
                        self.log("You squeal in pain.");
                        result = ActionResult::Pain;
                    }
                },
                _ => {}
            }
        }
        match result {
            ActionResult::Die => {
                self.log("You die.");
            },
            _ => {}
        }

        result
    }
}

fn load_game() -> GameState {
    let mut state = GameState {
        map: Vec::new(),
        history: vec!(String::from("You wake in a dark room...")),
        player: Player {
            x: MAX_MAP_W/2,
            y: MAX_MAP_H/2,
            health: 10,
        }
    };

    for i in 0..MAX_MAP_W {
        state.map.push(Vec::new());
        for j in 0..MAX_MAP_H {
            state.map[i].push(MapPoint{stack:Vec::new()});
        }
    }

    let (x, y) = coords(-5, 0);
    let p = &mut state.map[x][y].stack;
    p.push(MapObject::Knight);

    let (x, y) = coords(-4, -1);
    let p = &mut state.map[x][y].stack;
    p.push(MapObject::Fire);

    state
}

fn main() {
    let state = load_game();
    let mut game = GameInstance::new(state);

    for i in 0..game.state.history.len() {
        game.print_log(&format!("{}", game.state.history[i]));
    }

    loop {
        game.refresh();
        let action = game.prompt();
        match action {
            Action::Exit => break,
            _ => {
                match game.do_action(&action) {
                    ActionResult::Die => {
                        game.display.get_input();
                        break;
                    },
                    _ => {},
                }
            }
        }
    }
}
